#!/bin/bash
# Pleroma-Arch, a simple script to get a nicely configured BSPWM desktop up and running on Arch Linux
# Version 1.0.0, updated 2020-05-21
clear

bypass() {
  sudo -v
  while true;
  do
    sudo -n true
    sleep 45
    kill -0 "$$" || exit
  done 2>/dev/null &
}

echo "Welcome to Pleroma-Arch, a simple script to get a nicely configured BSPWM desktop up and running on Arch Linux."
read -p "Would you like to continue (y/n)? " installChoice

case "$installChoice" in
  # User wants to continue installation
  y|Y|yes|Yes|YES )

    # Make sure user is running as superuser
    echo "Enter your sudo password to continue."
    bypass

    read -p "Would you like to perform a system upgrade before continuing (y/n)? " upgradeChoice
    case "$upgradeChoice" in
      y/Y/yes/Yes/YES )
        echo "Upgrading system..."
        sudo pacman -Syu
      ;;
      n/N/no/No/NO )
        echo "Skipping system upgrade."
      ;;
    esac

    echo "Preparing to install packages..."
      echo "Installing build essentials and kernel headers..."
        sudo pacman -S linux-headers linux-tools-meta gcc ctags make cmake rsync yay
      # Check for an NVIDIA graphics card and install proprietary drivers if found
      case $(lspci -k | grep -A 2 -E "(VGA|3D)") in
        *NVIDIA*)
          musl=$(ldd /bin/ls | grep 'musl' | head -1 | cut -d ' ' -f1)
          if [ -z $musl ]; then
              # NVIDIA detect and user is on a Glibc system
              echo "Detected NVIDIA graphics card. Installing proprietary drivers..."
              sudo pacman -S nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader
          else
              # If user is on a system with Musl, driver installation is skipped.
              echo "NVIDIA proprietary drivers cannot be installed on a Musl system...skipping."
          fi
        ;;
      esac
      echo "Installing fonts..."
        sudo pacman -Sy fontconfig
        sudo yay -Sy nerd-fonts-iosevka ttf-material-design-icons
      echo "Installing audio packages..."
        sudo pacman -Sy alsa-utils pulseaudio-alsa ffmpeg lame pamixer pulseaudio x264
      echo "Installing desktop environment..."
        sudo yay -Sy bspwm dunst redshift rofi sxhkd rxvt-unicode betterlockscreen bottom cava polybar rofi sxhkd
      echo "Installing utilities and system tools..."
        sudo yay -Sy android-tools bind-tools curl dbus feh gvfs gvfs-mtp gzip lib32-gnutls lib32-libldap lib32-libgpg-error lib32-sqlite lib32-libpulse networkmanager ntp procps-ng udisks2 unrar unzip wget whois wireguard-dkms wireguard-tools youtube-dl zip
      echo "Installing additional applications..."
        # Edit the following list of additional applications or replace them with your own preferences
        # Code editor
        echo "Installing Atom..."
        sudo yay -Sy atom-editor-git
        # Audio editor
        echo "Installing Audacity..."
        sudo yay -Sy audacity
        # LED control for Corsair keyboards and mice
        echo "Installing ckb-next..."
        sudo yay -Sy ckb-next
        # Web browser
        echo "Installing Firefox..."
        sudo yay -Sy firefox
        # Screenshot utility
        echo "Installing Flameshot..."
        sudo yay -Sy flameshot
        # Image editor
        echo "Installing GIMP..."
        sudo yay -Sy gimp
        # Office suite
        echo "Installing LibreOffice..."
        sudo yay -Sy libreoffice-fresh
        # Audio and video player
        echo "Installing MPV..."
        sudo yay -Sy mpv
        # Text editor
        echo "Installing nano..."
        sudo pacman -Sy nano
        # Audio recording and streaming
        echo "Installing OBS Studio..."
        sudo yay -Sy obs-studio
        # File manager...atool is installed for the ranger_archives plugin to work properly
        echo "Installing Ranger..."
        sudo yay -Sy ranger atool
        # Voice conferencing
        echo "Installing Skype..."
        sudo yay -Sy skype
        # Gaming
        echo "Installing Lutris..."
        sudo yay -Sy lutris
        echo "Installing Steam..."
        sudo yay -Sy steam
        echo "Installing Wine..."
        sudo yay -Sy wine wine-32bit
        # PDF reader
        echo "Installing XPDF..."
        sudo yay -Sy xpdf
        # Install ZSH and Oh My ZSH
        echo "Installing ZSH..."
        sudo yay -Sy zsh
        sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        # Install Flatpak support and add Flathub repo
        echo "Installing Flatpak support..."
        sudo yay -Sy flatpak
        flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        # Install Spotify..."
        echo "Installing Spotify..."
        sudo yay -Sy spotify
        # Spotify ad-blocker
        sudo yay -Sy blockify

    echo "Configuring system..."
      echo "Updating nameservers..."
	sudo resolvconf -u
      echo "Configuring font settings..."
	sudo ln -s /usr/share/fontconfig/conf.avail/10-hinting-slight.conf /etc/fonts/conf.d/
	sudo ln -s /usr/share/fontconfig/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d/
      	sudo ln -s /usr/share/fontconfig/conf.avail/11-lcdfilter-default.conf /etc/fonts/conf.d/
	sudo ln -s /usr/share/fontconfig/conf.avail/50-user.conf /etc/fonts/conf.d/
	sudo ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/
      echo "Importing dotfiles..."
        # The following assumes your are running the script as stated in the README: bash pleroma-arch/install.sh. If you are running the script from inside the pleroma-arch directory (cd pleroma-arch; bash install.sh), change "cp -r pleroma-arch/home/* $HOME/" to "cp -r home/* $HOME/"
        shopt -s dotglob
        cp -r pleroma-arch/home/* $HOME/
        chmod +x $HOME/.config/ranger/scope.sh
        # Install Spicetify for Spotify theming support
        read -p "Would you like add Spotify theming support via Spicetify (y/n)? " spicetifyChoice
        case "$spicetifyChoice" in
          y/Y/yes/Yes/YES )
            sudo chmod a+wr /usr/share/spotify
            sudo chmod a+wr /usr/share/spotify/spotify-client
            sudo chmod a+wr /usr/share/spotify/spotify-client/Apps -R
            yay -Sy spicetify-cli
          ;;
          n/N/no/No/NO )
            rm -rf $HOME/.config/spicetify
            echo "Skipping Spicetify installation."
          ;;
        esac
        # END SPICETIFY INSTALL
      echo "Changing default shell to ZSH..."
        chsh -s /usr/bin/zsh $USER

    echo "All done! Please reboot for all changes to take effect."
  ;;

  # User does not want to continue installation.
  n|N|no|No|NO )
    echo "Thanks for trying, Pleroma Arch. Goodbye!";;
esac
