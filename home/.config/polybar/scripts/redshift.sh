#!/bin/bash

# Specifying the icon(s) in the script
# This allows us to change its appearance conditionally
icon=""

pgrep -x redshift &> /dev/null
if [[ $? -eq 0 ]]; then
    temp=$(redshift -p 2>/dev/null | grep temp | cut -d' ' -f3)
    temp=${temp//K/}
fi

# Grey out the widget when Redshift is not running, otherwise use the system color.
if [[ -z $temp ]]; then
    echo "$icon%{F#677} Off"       # Greyed out (not running)
else
    echo "$icon ${temp}K"
fi
